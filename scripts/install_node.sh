TMP_PATH=~/tmp_install_node

# Versions section
NODE_MAJOR=8.11
NODE_VERSION=$NODE_MAJOR.1

mkdir $TMP_PATH && cd $TMP_PATH

# Download and extract Node
wget --no-check-certificate https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.xz
tar -xf node-v$NODE_VERSION-linux-x64.tar.xz

# Install Node
mkdir -p /usr/local/lib/nodejs/node-v$NODE_VERSION-linux-x64
mv $TMP_PATH/node-v$NODE_VERSION-linux-x64 /usr/local/lib/nodejs/node-$NODE_VERSION
export NODEJS_HOME=/usr/local/lib/nodejs/node-$NODE_VERSION/bin
export PATH=$NODEJS_HOME:$PATH

# Finish installation
rm -rf $TMP_PATH
ln -s /usr/local/lib/nodejs/node-$NODE_VERSION/bin /usr/local/bin/node