TMP_PATH=~/tmp_install_jdk
# Versions section
JAVA_VERSION=8u171
JAVA_RPM_VERSION=8u171-b11
JAVA_RPM_HASH=512cd62ec5174c3487ac17c61aaa89e8

mkdir $TMP_PATH && cd $TMP_PATH

# Download JDK RPM
wget --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" \
        "http://download.oracle.com/otn-pub/java/jdk/${JAVA_RPM_VERSION}/${JAVA_RPM_HASH}/jdk-${JAVA_VERSION}-linux-x64.rpm"

# Install JDK
yum localinstall -y -q "jdk-${JAVA_VERSION}-linux-x64.rpm" && \
    rm "jdk-${JAVA_VERSION}-linux-x64.rpm"