TMP_PATH=~/tmp_install_cmake
# Versions section
CMAKE_MAJOR=3.11
CMAKE_VERSION=$CMAKE_MAJOR.1

mkdir $TMP_PATH && cd $TMP_PATH

# Download and extract Cmake
wget --no-check-certificate https://cmake.org/files/v$CMAKE_MAJOR/cmake-$CMAKE_VERSION.tar.gz
tar -zxvf cmake-$CMAKE_VERSION.tar.gz

# Compile Cmake
cd $TMP_PATH/cmake-$CMAKE_VERSION
./bootstrap
make
make install
rm -rf $TMP_PATH